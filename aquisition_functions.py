import numpy as np
from scipy.stats import mode
import sys


class rand_acq:
    def __init__(self, size):
        self.size = size
        self.reset()

    def reset(self):
        self.numbers = np.arange(self.size)
        np.random.shuffle(self.numbers)

    def score(self, output):
        num = len(output)

        ret = self.numbers[:num]
        self.numbers = self.numbers[num:]

        return ret

def var_ratio(output):
    classes = np.argmax(output, axis=-1)
    _, m = mode(classes, axis=-1)

    return 1 - m.squeeze(axis=-1)/output.shape[-2]


def entropy_max(output):
    mean_prob = output.mean(axis=-2)
    log_probability = np.log2(mean_prob + 1e-7)
    entropy = - np.multiply(mean_prob, log_probability)
    entropy = np.sum(entropy, axis=-1)

    return entropy


def bald_score(output):
    log_probability = np.log2(output + 1e-7)
    entropy = - np.multiply(output, log_probability)
    entropy = np.sum(entropy, axis=-1)
    entropy = np.mean(entropy, axis=-1)

    global_entropy = entropy_max(output)

    return global_entropy - entropy

def sig_to_softmax(output):
    exp = np.expand_dims(output, axis=3)

    return np.concatenate([1-exp, exp], axis=3)


def var_ratio_multi(output):
    rounded = np.round(output)
    rounded = (1 - (np.abs(rounded.mean(axis=1) - 0.5) + 0.5))#.mean(axis=1)

    return rounded


def entropy_max_multi(output):
    conv = sig_to_softmax(output)
    mean_prob = conv.mean(axis=1)
    log_probability = np.log2(mean_prob)
    entropy = - np.multiply(mean_prob, log_probability)
    entropy = np.sum(entropy, axis=2)#.mean(axis=1)

    return entropy


def bald_score_multi(output):
    conv = sig_to_softmax(output)

    log_probability = np.log2(conv + sys.float_info.epsilon)
    entropy = - np.multiply(conv, log_probability)
    entropy = np.sum(entropy, axis=3)
    entropy = np.mean(entropy, axis=1)

    mean_prob = conv.mean(axis=1)
    log_prob = np.log2(mean_prob)
    global_entropy = - np.multiply(mean_prob, log_prob)
    global_entropy = np.sum(global_entropy, axis=2)

    return (global_entropy - entropy)#.mean(axis=1)


def random_order_score(output):
    out = np.arange(output.shape[0])
    np.random.shuffle(out)

    return out

def variance(output):
    return np.var(output, axis=-2).mean(axis=-1)

