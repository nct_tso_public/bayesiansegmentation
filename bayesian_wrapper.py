import torch
import torch.nn as nn

class BayesianWrapper(nn.Module):
    APPLY_TO = [nn.Linear, nn.Conv2d, nn.ConvTranspose2d]

    class BayesianDropout(nn.Module):
        def __init__(self, p=0.2, inplace=False):
            super(BayesianWrapper.BayesianDropout, self).__init__()
            self.dropout = nn.Dropout(p=p, inplace=inplace)

        def forward(self, input):
            return self.dropout.forward(input)

        def train(self, mode=True):
            """
            train/eval settings have no effect on bayesian dropouts
            :param mode:
            :return:
            """
            return self

    class BayesianSequential(nn.Sequential):
        def __init__(self, layer):
            super(BayesianWrapper.BayesianSequential, self).__init__(BayesianWrapper.BayesianDropout(),
                                                                     layer)

    def __init__(self, model):
        """
        :param model:
        :type model: nn.Model
        """
        super(BayesianWrapper, self).__init__()
        self.model = model
        self.model.apply(BayesianWrapper.make_bayesian)
        self.is_bayesian = True

    def make_bayesian(self):
        for key in self._modules:
            if type(self._modules[key]) in BayesianWrapper.APPLY_TO:
                self._modules[key] = BayesianWrapper.BayesianSequential(self._modules[key])

    def remove_bayesian(self):
        for key in self._modules:
            if type(self._modules[key]) is BayesianWrapper.BayesianSequential:
                self._modules[key] = self._modules[key]._modules["1"]

    def bayesian(self, mode=True):
        if self.is_bayesian and not mode:
            self.model.apply(BayesianWrapper.remove_bayesian)
        if not self.is_bayesian and mode:
            self.model.apply(BayesianWrapper.make_bayesian)
        self.is_bayesian = mode

    def load_state_dict(self, state_dict, strict=True):
        self.model.apply(BayesianWrapper.remove_bayesian)
        self.model.load_state_dict(state_dict, strict=strict)
        if self.is_bayesian:
            self.model.apply(BayesianWrapper.make_bayesian)

    def state_dict(self, destination=None, prefix='', keep_vars=False):
        self.model.apply(BayesianWrapper.remove_bayesian)
        state_dict = self.model.state_dict(destination=destination, prefix=prefix, keep_vars=keep_vars)
        if self.is_bayesian:
            self.model.apply(BayesianWrapper.make_bayesian)
        return state_dict

    def load(self, model_file):
        self.load_state_dict(torch.load(model_file))

    def save(self, model_file):
        torch.save(self.state_dict(), model_file)

    def forward(self, *input,**kwargs):
        return self.model.forward(*input,kwargs)

    def __repr__(self):
        return self.model.__repr__()

if __name__ == '__main__':
    import torchvision.models as mod
    import torch

    print("running tests:")
    for net in [mod.alexnet, mod.resnet18, mod.DenseNet]:
        print("   ", net.__name__)
        ba = BayesianWrapper(net())

        # Test state_dict compatibility
        ba.load_state_dict(net().state_dict())
        net().load_state_dict(ba.state_dict())
        assert(net().state_dict().keys() == ba.state_dict().keys())

        # Test train/eval settings
        t = torch.randn((1,3,224,224))
        assert(torch.eq(ba(t), ba(t)).all().item() == 0) #active dropouts & bayesian dropouts
        ba.eval()
        assert(torch.eq(ba(t), ba(t)).all().item() == 0) #active bayesian dropouts
        ba.bayesian(False)
        assert(torch.eq(ba(t), ba(t)).all().item() == 1) #3 no active dropout
        ba.train()
        assert(torch.eq(ba(t), ba(t)).all().item() == int(net.__name__!='alexnet')) #4 active dropout (only exist in alexnet)
        ba.bayesian()
        assert(torch.eq(ba(t), ba(t)).all().item() == 0) #active dropouts & bayesian dropouts


    print("succes!")

    print(BayesianWrapper(mod.alexnet()))
    print(BayesianWrapper(mod.alexnet()).state_dict().keys())