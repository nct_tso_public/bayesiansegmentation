from __future__ import print_function
from PIL import Image
import os
import os.path
import errno
import numpy as np
import torch.utils.data as data
import csv
import scipy.ndimage.interpolation
import skimage.color
import skimage.transform
import matplotlib.pyplot as plt
import torch
import glob
import random


class DatasetEndoVis17(data.Dataset):
    def __init__(self, image_path, annotation_path, width, height, transform=None, target_transform=None, num_class=2, labeled_map = None):
        self.transform = transform
        self.target_transform = target_transform
        self.target = []
        self.width = width
        self.height = height
        self.img = []
        self.setMap(labeled_map)
        print(image_path)
        files = glob.glob(os.path.join(image_path, '*.png'))
        paths = glob.glob(annotation_path + '/*/')
        print(len(files))
        for i in range(len(files)):
            frame = image_path + "/frame%03d.png" % i

            img_frame = Image.open(frame)
            target = np.zeros((height, width), dtype=np.int64)
            img_frame = self.resize(img_frame, mode=Image.BILINEAR)
            self.img.append(img_frame)

            for p in paths:
                label = p + "/frame%03d.png" % i
                img_label = Image.open(label)
                img_label = self.resize(img_label).convert('L')
                img_np = np.asarray(img_label)//10
                img_np[img_np > 4] = 0
                target[img_np > 0] = img_np[img_np > 0]

            #print(np.max(target))
            if num_class < 5:
                target[target > 1] = 1

            self.target.append(target)

    def resize(self, image, width=None, height=None, mode=Image.NEAREST):
        if width is None:
            width = self.width

        if height is None:
            height = self.height
        image = image.crop((320,28, 1600,1052))
        w = image.width
        h = image.height
        height2 = int(width * (h / w))

        offset_y = (height - height2) // 2

        image_y = image.resize((width, height2), resample=mode)
        image_2 = Image.new('RGB', (width, height), (0, 0, 0))
        image_2.paste(image_y, box=(0, offset_y))

        return image_2

    def __getitem__(self, index_):
        if self.label_map is None:
            index = index_
        else:
            index = self.indices[index_]
            #print(index,self.label_map[index])
            if self.label_map[index] != 1:
                print("Index error!!")
        img = self.img[index]

        if self.transform is not None:
            img = self.transform(img)

        target = self.target[index]

        if self.target_transform is not None:
            target = self.target_transform(target)

        return img, target

    def __len__(self):
        if self.label_map is None:
            return len(self.img)
        else:
            return np.sum(self.label_map)

    def setMap(self, map):
        if map is not None:
            self.indices = np.nonzero(map)[0]

        self.label_map = map


class DatasetImage2Image(data.Dataset):
    def __init__(self, image_path, annotation_path, width, height, number_images, transform=None, target_transform=None):
        self.transform = transform
        self.target_transform = target_transform
        self.target = []
        self.width = width
        self.height = height
        self.img = []
        self.styles = 5
        self.files = 2000
        self.number_images = number_images
        print(image_path)

        for s in range(self.styles):
            for i in range(self.files):

                img_frame = image_path + "/style_%02d/img%05d.png" % (s,i)
                self.img.append(img_frame)
                label = annotation_path + "/lbl%05d.png" % i

                self.target.append(label)

    def open_image(self, image_file, width=None, height=None, mode=Image.NEAREST):
        if width is None:
            width = self.width

        if height is None:
            height = self.height
        with Image.open(image_file) as image:
            w = image.width
            h = image.height
            height2 = int(width * (h / w))

            offset_y = (height - height2) // 2

            image_y = image.resize((width, height2), resample=mode)
            image_2 = Image.new('RGB', (width, height), (0, 0, 0))
            image_2.paste(image_y, box=(0, offset_y))
            image_y.close()
            
        return image_2

    def __getitem__(self, index_):
        index = random.randint(0, self.number_images - 1)
        img = self.open_image(self.img[index])

        if self.transform is not None:
            img = self.transform(img)
    
        target = np.zeros((self.height, self.width), dtype=np.int64)
        img_label = self.open_image(self.target[index]).convert('L')
        img_np = np.asarray(img_label)//26
        #print(np.unique(img_np))

        target[img_np > 0] = img_np[img_np > 0]
        #print(np.min(target),np.max(target))

        if self.target_transform is not None:
            target = self.target_transform(target)

        img_label.close()
        return img, target

    def __len__(self):
        return self.number_images

