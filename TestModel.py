import sys
import torch
import torchvision
import torchvision.transforms as transforms
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import models
import loss
import Dataset
import os.path
import datetime
from shutil import copy2
import numpy as np
from PIL import Image
from bayesian_wrapper import BayesianWrapper
from sklearn.metrics import f1_score
import aquisition_functions

criteria = [aquisition_functions.var_ratio,
            aquisition_functions.entropy_max,
            aquisition_functions.bald_score,
            aquisition_functions.variance]


if len(sys.argv) != 3:
    print("Please provide the path to the model and number of classes (2 or 5)")
    exit()

#num_class = 5 #5 for part-based segmentation, 2 for binary
num_class = int(sys.argv[2])
#convert prediction to image
def toImage(input,input2_s,input3_s,n_Class):
    input2 = input2_s.reshape((input2_s.shape[0], input2_s.shape[1], 1))
    input3 = input3_s.reshape((input3_s.shape[0], input3_s.shape[1], 1))

    combined = np.concatenate([input2, input2,input2], axis=2)
    combined2 = np.concatenate([input3, input3, input3], axis=2)
    conv_input = (input.numpy().transpose((1,2,0)) * [0.229, 0.224, 0.225]) + [0.485, 0.456, 0.406]

    combined = np.concatenate([conv_input*255, combined* (255 / n_Class), combined2* (255 / n_Class)], axis=1)
    img = combined.astype(np.uint8)
    image = Image.fromarray(img, 'RGB')

    return image

#input size to CNN
width = 320
height = 256

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
device_cpu = torch.device("cpu")

# Assume that we are on a CUDA machine, then this should print a CUDA device:

print(device)

data_folder  = "/media/data/sebastian/RIS/" # path to dataset

output_folder = "/media/data/sebastian/RIS/Res/" #path to output results and models
test_sequences = ["instrument_dataset_7",
                  "instrument_dataset_8"]

model_file = sys.argv[1]

output_folder += datetime.datetime.now().strftime("%Y%m%d-%H%M") + "/"
os.makedirs(output_folder)
copy2(os.path.realpath(__file__),output_folder)

batch_size = 10
epochs = 50
bayesian_iter = 10

test_loaders = []

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
transform = transforms.Compose(
     [transforms.ToTensor(),
     normalize])

for i in range(len(test_sequences)):
    images = data_folder + test_sequences[i]
    set = Dataset.DatasetEndoVis17(images + "/left_frames/", images + "/ground_truth/", width, height, transform,num_class=num_class)
    loader = torch.utils.data.DataLoader(set, batch_size=batch_size, shuffle=False, num_workers=2)
    test_loaders.append(loader)

f_log = open(output_folder + "log.txt", "w")

bce_loss = loss.LossMulti(jaccard_weight=0.5, num_classes=num_class)

orig_net = models.UNet11(num_classes=num_class, pretrained=True) #Initialize TernausNet
net = BayesianWrapper(orig_net) # Convert to Bayesian CNN
net.load(model_file)
net.to(device)

str = ""
with torch.no_grad():
    for l in range(len(test_loaders)):
        test_count = 0
        test_loss = 0
        test_batch = 0
        test_accuracy = 0
        test_outputs = []
        test_labels = []

        all_criteria = []

        for data in test_loaders[l]:

            images_cpu, labels_cpu = data
            images = images_cpu.to(device)
            labels = labels_cpu.to(device)

            out = []
            for t in range(bayesian_iter):
                output = net(images, no_log=True)
                out.append(output)
                #if test_batch == 0:
                #    print(output.to(device_cpu)[0,:,128,160])

            out = torch.stack(out, 1)
            outputs = torch.mean(out,dim=1)

            output_reshape = out.reshape(out.size(0), out.size(1), out.size(2), -1).to(device_cpu).numpy().transpose((0, 3, 1,2))
            res_criteria = []

            for c in criteria:
                res_criteria.append(c(output_reshape).mean(axis=1))

            res_criteria = np.stack(res_criteria, axis=1)
            all_criteria.append(res_criteria)

            c_loss = bce_loss(outputs, labels)

            test_loss += c_loss.item()
            _, predicted = torch.max(outputs.data, 1)

            test_outputs.append(predicted.view(-1).cpu().numpy())
            test_labels.append(labels.view(-1).cpu().numpy())

            test_accuracy += (predicted == labels).sum().item()
            test_batch += 1

            predicted_cpu = predicted.to(device_cpu)

            if test_batch == 0:
                img = toImage(images_cpu[0],labels_cpu[0], predicted_cpu[0], num_class)
                img.save(output_folder + "test%02d.png" % (l))

            test_count += labels_cpu.size(0) * labels_cpu.size(1) * labels_cpu.size(2)

        test_outputs = np.concatenate(test_outputs, axis=0)
        test_labels = np.concatenate(test_labels, axis=0)
        test_dsc = f1_score(test_labels, test_outputs, average='macro')

        all_criteria = np.concatenate(all_criteria)
        np.savetxt(output_folder + "Uncertainties_%02d.csv" % (l),all_criteria,delimiter=",")

        str += " test %d (loss %.3f, accuracy %.3f dice %.3f)" % (
        l, test_loss / test_batch, test_accuracy / (test_count), test_dsc)
    print(str)
    f_log.write(str + "\n")
f_log.close()
