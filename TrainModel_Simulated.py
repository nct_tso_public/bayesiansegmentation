import sys
import torch
import torchvision
import torchvision.transforms as transforms
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import models
import loss
import Dataset
import os.path
import datetime
from shutil import copy2
import numpy as np
from PIL import Image
from bayesian_wrapper import BayesianWrapper
from sklearn.metrics import f1_score

num_class = 7

#convert prediction to image
def toImage(input,input2_s,input3_s,n_Class):
    input2 = input2_s.reshape((input2_s.shape[0], input2_s.shape[1], 1))
    input3 = input3_s.reshape((input3_s.shape[0], input3_s.shape[1], 1))

    combined = np.concatenate([input2, input2,input2], axis=2)
    combined2 = np.concatenate([input3, input3, input3], axis=2)
    conv_input = (input.numpy().transpose((1,2,0)) * [0.229, 0.224, 0.225]) + [0.485, 0.456, 0.406]

    combined = np.concatenate([conv_input*255, combined* (255 / n_Class), combined2* (255 / n_Class)], axis=1)
    img = combined.astype(np.uint8)
    image = Image.fromarray(img, 'RGB')

    return image

#input size to CNN
width = 320
height = 256
image_per_seq = 1000

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
device_cpu = torch.device("cpu")

# Assume that we are on a CUDA machine, then this should print a CUDA device:

print(device)

#data_folder  = "/media/data_local/Image2Image/" # path to dataset
data_folder  = "/local_home/bodenstse/Image2Image/" # path to dataset

output_folder = "/media/data/sebastian/Active_Segmentation/Results/" #path to output results and models
train_sequences = ["3Dircadb1.1",
                   "3Dircadb1.2",
                   "3Dircadb1.8",
                   "3Dircadb1.9",
                   "3Dircadb1.10",
                   "3Dircadb1.11",
                   "3Dircadb1.17",
                   "3Dircadb1.18",
                   "3Dircadb1.19",
                   "3Dircadb1.20"]

output_folder += datetime.datetime.now().strftime("%Y%m%d-%H%M") + "/"
os.makedirs(output_folder, exist_ok=True)
copy2(os.path.realpath(__file__),output_folder)

batch_size = 10
epochs = 50

train_loaders = []
test_loaders = []

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
transform = transforms.Compose(
     [transforms.ToTensor(),
     normalize])

for i in range(len(train_sequences)):
    image_data = data_folder + "styleFromCholec80/" + train_sequences[i]
    image_annotation = data_folder + "labels/simulated/" + train_sequences[i] + "/labels/"
    set = Dataset.DatasetImage2Image(image_data, image_annotation, width, height, image_per_seq, transform)
    loader = torch.utils.data.DataLoader(set, batch_size=batch_size, shuffle=False, num_workers=2)
    train_loaders.append(loader)

f_log = open(output_folder + "log.txt", "w")

bce_loss = loss.LossMulti(jaccard_weight=0.5, num_classes=num_class)

orig_net = models.UNet11(num_classes=num_class, pretrained=True) #Initialize TernausNet
net = BayesianWrapper(orig_net) # Convert to Bayesian CNN
net.to(device)

optimizer = optim.Adam(net.parameters(), lr=1e-4)

for e in range(epochs):
    train_loss = 0
    train_accuracy = 0
    train_batch = 0

    train_count = 0
    train_outputs = []
    train_labels = []

    train_dsc = 0

    for l in range(len(train_loaders)):
        for data in train_loaders[l]:
            images_cpu, labels_cpu = data
            images = images_cpu.to(device)
            labels = labels_cpu.to(device)

            optimizer.zero_grad()
            outputs = net(images)
            
            c_loss = bce_loss(outputs, labels)
            c_loss.backward()
            optimizer.step()

            train_loss += c_loss.item()

            _, predicted = torch.max(outputs.data, 1)

            train_outputs = (predicted.view(-1).cpu().numpy())
            train_labels = (labels.view(-1).cpu().numpy())

            train_accuracy += (predicted == labels).sum().item()
            train_batch +=1

            predicted_cpu = predicted.to(device_cpu)
            train_count += labels_cpu.size(0) * labels_cpu.size(1) * labels_cpu.size(2)

            if train_batch % 10 == 0:
                img = toImage(images_cpu[0],labels_cpu[0], predicted_cpu[0], num_class)
                img.save(output_folder + "train%d_%04d.png" % (e,train_batch))

            train_dsc += f1_score(train_labels, train_outputs, average='macro')

    str = "Ep %d Train (loss %.3f, accuracy %.3f dice %.3f)" % (e,
    train_loss / train_batch, train_accuracy / (train_count), train_dsc / train_batch)

    print(str)
    f_log.write(str + "\n")
f_log.close()
net.save(output_folder + "model.npz")
