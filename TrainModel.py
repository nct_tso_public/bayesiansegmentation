import sys
import torch
import torchvision
import torchvision.transforms as transforms
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import models
import loss
import Dataset
import os.path
import datetime
from shutil import copy2
import numpy as np
from PIL import Image
from bayesian_wrapper import BayesianWrapper
from sklearn.metrics import f1_score

num_class = 2 #5 for part-based segmentation, 2 for binary

#convert prediction to image
def toImage(input,input2_s,input3_s,n_Class):
    input2 = input2_s.reshape((input2_s.shape[0], input2_s.shape[1], 1))
    input3 = input3_s.reshape((input3_s.shape[0], input3_s.shape[1], 1))

    combined = np.concatenate([input2, input2,input2], axis=2)
    combined2 = np.concatenate([input3, input3, input3], axis=2)
    conv_input = (input.numpy().transpose((1,2,0)) * [0.229, 0.224, 0.225]) + [0.485, 0.456, 0.406]

    combined = np.concatenate([conv_input*255, combined* (255 / n_Class), combined2* (255 / n_Class)], axis=1)
    img = combined.astype(np.uint8)
    image = Image.fromarray(img, 'RGB')

    return image

#input size to CNN
width = 320
height = 256

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
device_cpu = torch.device("cpu")

# Assume that we are on a CUDA machine, then this should print a CUDA device:

print(device)

data_folder  = "/media/data/sebastian/RIS/" # path to dataset

output_folder = "/media/data/sebastian/RIS/Res/" #path to output results and models
train_sequences = ["instrument_dataset_1",
                   "instrument_dataset_2",
                   "instrument_dataset_3",
                   "instrument_dataset_4",
                   "instrument_dataset_5",
                   "instrument_dataset_6",]
test_sequences = ["instrument_dataset_7",
                  "instrument_dataset_8"]

output_folder += datetime.datetime.now().strftime("%Y%m%d-%H%M") + "/"
os.makedirs(output_folder)
copy2(os.path.realpath(__file__),output_folder)

batch_size = 10
epochs = 50

train_loaders = []
test_loaders = []

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
transform = transforms.Compose(
     [transforms.ToTensor(),
     normalize])

for i in range(len(train_sequences)):
    images = data_folder + train_sequences[i]
    set = Dataset.DatasetEndoVis17(images + "/left_frames/", images + "/ground_truth/", width, height, transform,num_class=num_class)
    loader = torch.utils.data.DataLoader(set, batch_size=batch_size, shuffle=False, num_workers=2)
    train_loaders.append(loader)

for i in range(len(test_sequences)):
    images = data_folder + test_sequences[i]
    set = Dataset.DatasetEndoVis17(images + "/left_frames/", images + "/ground_truth/", width, height, transform,num_class=num_class)
    loader = torch.utils.data.DataLoader(set, batch_size=batch_size, shuffle=False, num_workers=2)
    test_loaders.append(loader)

f_log = open(output_folder + "log.txt", "w")

bce_loss = loss.LossMulti(jaccard_weight=0.5, num_classes=num_class)

orig_net = models.UNet11(num_classes=num_class, pretrained=True) #Initialize TernausNet
net = BayesianWrapper(orig_net) # Convert to Bayesian CNN
net.to(device)

optimizer = optim.Adam(net.parameters(), lr=1e-4)

for e in range(epochs):
    train_loss = 0
    train_accuracy = 0
    train_batch = 0

    train_count = 0
    train_outputs = []
    train_labels = []

    for l in range(len(train_loaders)):
        for data in train_loaders[l]:
            images_cpu, labels_cpu = data
            images = images_cpu.to(device)
            labels = labels_cpu.to(device)

            optimizer.zero_grad()
            outputs = net(images)

            c_loss = bce_loss(outputs, labels)
            c_loss.backward()
            optimizer.step()

            train_loss += c_loss.item()

            _, predicted = torch.max(outputs.data, 1)

            train_outputs.append(predicted.view(-1).cpu().numpy())
            train_labels.append(labels.view(-1).cpu().numpy())

            train_accuracy += (predicted == labels).sum().item()
            train_batch +=1

            predicted_cpu = predicted.to(device_cpu)
            train_count += labels_cpu.size(0) * labels_cpu.size(1) * labels_cpu.size(2)

            if train_batch % 10 == 0:
                img = toImage(images_cpu[0],labels_cpu[0], predicted_cpu[0], num_class)
                img.save(output_folder + "train%d_%04d.png" % (e,train_batch))

    train_outputs = np.concatenate(train_outputs,axis=0)
    train_labels = np.concatenate(train_labels,axis=0)
    train_dsc = f1_score(train_labels, train_outputs, average='macro')

    str = "Ep %d Train (loss %.3f, accuracy %.3f dice %.3f)" % (e,
    train_loss / train_batch, train_accuracy / (train_count), train_dsc)

    with torch.no_grad():
        for l in range(len(test_loaders)):
            test_count = 0
            test_loss = 0
            test_batch = 0
            test_accuracy = 0
            test_outputs = []
            test_labels = []

            for data in test_loaders[l]:

                images_cpu, labels_cpu = data
                images = images_cpu.to(device)
                labels = labels_cpu.to(device)

                outputs = net(images)

                c_loss = bce_loss(outputs, labels)

                test_loss += c_loss.item()
                _, predicted = torch.max(outputs.data, 1)

                test_outputs.append(predicted.view(-1).cpu().numpy())
                test_labels.append(labels.view(-1).cpu().numpy())

                test_accuracy += (predicted == labels).sum().item()
                test_batch += 1

                predicted_cpu = predicted.to(device_cpu)

                if test_batch == 0:
                    img = toImage(images_cpu[0],labels_cpu[0], predicted_cpu[0], num_class)
                    img.save(output_folder + "test%03d_%02d.png" % (e,l))

                test_count += labels_cpu.size(0) * labels_cpu.size(1) * labels_cpu.size(2)

            test_outputs = np.concatenate(test_outputs, axis=0)
            test_labels = np.concatenate(test_labels, axis=0)
            test_dsc = f1_score(test_labels, test_outputs, average='macro')


            str += " test %d (loss %.3f, accuracy %.3f dice %.3f)" % (
            l, test_loss / test_batch, test_accuracy / (test_count), test_dsc)
    print(str)
    f_log.write(str + "\n")
f_log.close()
net.save(output_folder + "model.npz")
